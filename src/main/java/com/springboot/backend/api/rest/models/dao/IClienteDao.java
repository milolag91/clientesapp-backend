package com.springboot.backend.api.rest.models.dao;

import com.springboot.backend.api.rest.models.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface IClienteDao extends CrudRepository<Cliente, Long> {
}
